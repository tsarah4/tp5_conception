Pour ce projet, il vous faut installer les dépendances :

```
pip install -r requirements.txt
```

Pour lancer le code :

```
uvicorn main::app --reload
```

Pour savoir si un aliment est végan ou non : 

Taper l'URL :

http://127.0.0.1:8000/items/ + code de l'aliment

Par exemple :

La brioche n'est pas végane :

http://127.0.0.1:8000/items/3256540001305

La rozana est végane :

http://127.0.0.1:8000/items/3468570116601